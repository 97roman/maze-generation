﻿using CodeBase.Extensions;
using UnityEngine;


namespace CodeBase
{
    public class CameraFollow : MonoBehaviour
    {
        public Vector3 Rotation;
        public float Distance;
        public float OffsetY;

        public Transform FollowTarget;

        private void LateUpdate()
        {
            if (FollowTarget == null)
                return;

            var rotation = Quaternion.Euler(Rotation);
            var position = rotation * new Vector3(0, 0, -Distance) + FollowTarget.position.AddY(OffsetY);

            transform.rotation = rotation;
            transform.position = position;
        }
    }
}