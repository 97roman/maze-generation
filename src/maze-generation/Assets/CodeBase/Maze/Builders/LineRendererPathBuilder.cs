﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;


namespace CodeBase.Maze.Builders
{
    [RequireComponent(typeof(LineRenderer))]
    public class LineRendererPathBuilder : MonoBehaviour
    {
        private LineRenderer _lineRenderer;

        private void Awake()
        {
            _lineRenderer = GetComponent<LineRenderer>();
        }

        public void BuildPath(IEnumerable<IMazeCell> path)
        {
            var positions = path.Select(cell => cell.PhysicalCell.LocalPathPoint).ToArray();
            _lineRenderer.positionCount = positions.Length;
            _lineRenderer.SetPositions(positions);
        }
    }
}