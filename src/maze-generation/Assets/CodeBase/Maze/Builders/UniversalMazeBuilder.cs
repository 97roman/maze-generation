﻿using System;
using CodeBase.Maze.Creators;
using UnityEngine;


namespace CodeBase.Maze.Builders
{
    public class UniversalMazeBuilder
    {
        private readonly MazeFormType _formType;

        private readonly RectMazeBuilder _rectMazeBuilder;
        private readonly TriangleMazeBuilder _triangleMazeBuilder;
        

        public UniversalMazeBuilder(MazeFormType formType, GameObject mazeCellPrefab, GameObject mazeExitPrefab)
        {
            _formType = formType;

            switch (_formType)
            {
                case MazeFormType.Square:
                    _rectMazeBuilder = new RectMazeBuilder(mazeCellPrefab, mazeExitPrefab);
                    break;
                
                case MazeFormType.Triangle:
                    _triangleMazeBuilder = new TriangleMazeBuilder(mazeCellPrefab, mazeExitPrefab);
                    break;
                
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        public void BuildMaze(IMaze maze, Transform parent)
        {
            switch (_formType)
            {
                case MazeFormType.Square:
                    _rectMazeBuilder.BuildMaze((RectMaze) maze, parent);
                    break;
                
                case MazeFormType.Triangle:
                    _triangleMazeBuilder.BuildMaze((TriangleMaze) maze, parent);
                    break;
                
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }
    }
}