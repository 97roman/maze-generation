﻿using UnityEngine;


namespace CodeBase.Maze.Builders
{
    public class TrianglePhysicalMazeCell : MonoBehaviour, IPhysicalCell
    {
        public Vector3 Size;
        public Vector3 RotationMask;
        public GameObject LeftWall;
        public GameObject RightWall;
        public GameObject BottomWall;

        public Vector3 LocalPathPoint => transform.localPosition + transform.localRotation * Offset;
        public Vector3 WorldPathPoint => transform.position + transform.rotation * Offset;

        private Vector3 Offset
        {
            get
            {
                var leftWallInactive = !LeftWall.activeSelf;
                var rightWallInactive = !RightWall.activeSelf;
                var bottomWallInactive = !BottomWall.activeSelf;
                
                if (leftWallInactive && rightWallInactive && bottomWallInactive)
                    return Vector3.zero;
                
                if (leftWallInactive && bottomWallInactive)
                    return new Vector3(-0.125f * Size.x, -0.25f * Size.y, -0.25f * Size.z);
                
                if (rightWallInactive && bottomWallInactive)
                    return new Vector3(0.125f * Size.x, -0.25f * Size.y, -0.25f * Size.z);
                
                return Vector3.zero;
            }
        }
    }
}