﻿using System;
using UnityEngine;


namespace CodeBase.Maze.Builders
{
    [RequireComponent(typeof(TriggerObserver))]
    public class MazeExit : MonoBehaviour
    {
        private TriggerObserver _triggerObserver;


        private void Awake()
        {
            _triggerObserver = GetComponent<TriggerObserver>();
            _triggerObserver.TriggerEnter += TriggerEnter;
        }

        private void OnDestroy()
        {
            _triggerObserver.TriggerEnter -= TriggerEnter;
        }

        private void TriggerEnter(Component obj)
        {
#if UNITY_EDITOR
            UnityEditor.EditorUtility.DisplayDialog("Win Window", "You win!", "ok");
#endif
        }
    }
}
