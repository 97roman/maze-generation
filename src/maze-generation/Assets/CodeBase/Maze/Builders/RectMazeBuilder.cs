﻿using CodeBase.Maze.Creators;
using UnityEngine;


namespace CodeBase.Maze.Builders
{
    public class RectMazeBuilder
    {
        private readonly GameObject _mazeCellPrefab;
        private readonly GameObject _mazeExitPrefab;


        public RectMazeBuilder(GameObject mazeCellPrefab, GameObject mazeExitPrefab)
        {
            _mazeCellPrefab = mazeCellPrefab;
            _mazeExitPrefab = mazeExitPrefab;
        }

        public void BuildMaze(RectMaze maze, Transform parent)
        {
            var cellSize = _mazeCellPrefab.GetComponent<RectPhysicalMazeCell>().Size;
            cellSize = Vector3.Scale(cellSize, _mazeCellPrefab.transform.localScale);

            for (var x = 0; x < maze.Cells.GetLength(0); x++)
            {
                for (var y = 0; y < maze.Cells.GetLength(1); y++)
                {
                    var cell = maze.Cells[x, y];

                    var cellObject = Object.Instantiate(_mazeCellPrefab, parent);
                    var cellPosition = new Vector3(x * cellSize.x, y * cellSize.y, y * cellSize.z);
                    cellObject.transform.localPosition = cellPosition;

                    var physicalCell = cellObject.GetComponent<RectPhysicalMazeCell>();
                    physicalCell.LeftWall.SetActive(cell.LeftNeighbor.Wall.IsExists);
                    physicalCell.RightWall.SetActive(cell.RightNeighbor.Wall.IsExists);
                    physicalCell.TopWall.SetActive(cell.TopNeighbor.Wall.IsExists);
                    physicalCell.BottomWall.SetActive(cell.BottomNeighbor.Wall.IsExists);

                    cell.PhysicalCell = physicalCell;
                }
            }
            
            var exit = Object.Instantiate(_mazeExitPrefab, parent);
            exit.transform.localPosition = maze.Exit.PhysicalCell.LocalPathPoint;
        }
    }
}