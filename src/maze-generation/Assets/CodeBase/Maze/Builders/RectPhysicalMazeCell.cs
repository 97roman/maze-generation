﻿using UnityEngine;


namespace CodeBase.Maze.Builders
{
    public class RectPhysicalMazeCell : MonoBehaviour, IPhysicalCell
    {
        public Vector3 Size;
        public GameObject LeftWall;
        public GameObject RightWall;
        public GameObject TopWall;
        public GameObject BottomWall;
        
        public Vector3 LocalPathPoint => transform.localPosition;
        public Vector3 WorldPathPoint => transform.position;
    }
}