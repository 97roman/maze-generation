﻿using CodeBase.Maze.Creators;
using UnityEngine;


namespace CodeBase.Maze.Builders
{
    public class TriangleMazeBuilder
    {
        private readonly GameObject _mazeCellPrefab;
        private readonly GameObject _mazeExitPrefab;

        public TriangleMazeBuilder(GameObject mazeCellPrefab, GameObject mazeExitPrefab)
        {
            _mazeCellPrefab = mazeCellPrefab;
            _mazeExitPrefab = mazeExitPrefab;
        }

        public void BuildMaze(TriangleMaze maze, Transform parent)
        {
            var cellComponent = _mazeCellPrefab.GetComponent<TrianglePhysicalMazeCell>();
            
            var cellSize = Vector3.Scale(cellComponent.Size, _mazeCellPrefab.transform.localScale);
            var upsideDownRotation = Quaternion.Euler(Vector3.Scale(new Vector3(180f, 180f, 180f), cellComponent.RotationMask));

            for (var x = 0; x < maze.Cells.Length; x++)
            {
                for (var y = 0; y < maze.Cells[x].Length; y++)
                {
                    var cell = maze.Cells[x][y];

                    var cellObject = Object.Instantiate(_mazeCellPrefab, parent);
                    var cellPosition = new Vector3(x * 0.5f * cellSize.x, y * cellSize.y, y * cellSize.z);
                    cellObject.transform.localPosition = cellPosition;

                    if (cell.IsUpsideDown)
                        cellObject.transform.localRotation = upsideDownRotation;

                    var physicalCell = cellObject.GetComponent<TrianglePhysicalMazeCell>();
                    physicalCell.LeftWall.SetActive(cell.LeftNeighbor.Wall.IsExists);
                    physicalCell.RightWall.SetActive(cell.RightNeighbor.Wall.IsExists);
                    physicalCell.BottomWall.SetActive(cell.BottomNeighbor.Wall.IsExists);

                    cell.PhysicalCell = physicalCell;
                }
            }

            var exit = Object.Instantiate(_mazeExitPrefab, parent);
            exit.transform.localPosition = maze.Exit.PhysicalCell.LocalPathPoint;
        }
    }
}