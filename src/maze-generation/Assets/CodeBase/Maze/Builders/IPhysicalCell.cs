﻿using UnityEngine;


namespace CodeBase.Maze.Builders
{
    public interface IPhysicalCell
    {
        Vector3 LocalPathPoint { get; }
        Vector3 WorldPathPoint { get; }
    }
}