﻿using CodeBase.Extensions;
using CodeBase.Maze.Builders;
using CodeBase.Maze.Creators;
using CodeBase.Maze.Generators;
using UnityEngine;


namespace CodeBase.Maze.Spawners
{
    public class RectMazeSpawner : MonoBehaviour
    {
        public int MazeWidth;
        public int MazeHeight;
        public GameObject MazeCellPrefab;
        public GameObject MazeExitPrefab;
        public GameObject PlayerPrefab;
        public LineRendererPathBuilder PathBuilder;
        

        private void Start()
        {
            var mazeCreator = new RectMazeCreator(MazeWidth, MazeHeight);
            var maze = mazeCreator.CreateMaze();
            
            maze.SetRandomEntranceAtEdge();
            var generator = new RecursiveBacktrackerGenerator();
            generator.Generate(maze);
            maze.SetFarthestExitAtEdge();

            var builder = new RectMazeBuilder(MazeCellPrefab, MazeExitPrefab);
            builder.BuildMaze(maze, transform);
            
            var player = Instantiate(PlayerPrefab);
            player.transform.position = maze.Entrance.PhysicalCell.WorldPathPoint;
            
            PathBuilder.BuildPath(maze.GetWalkthroughPath());
        }
    }
}
