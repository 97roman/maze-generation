﻿using System.Collections.Generic;
using CodeBase.Maze.Builders;


namespace CodeBase.Maze.Creators
{
    public class TriangleMazeCell : IMazeCell
    {
        public MazeNeighbor LeftNeighbor;
        public MazeNeighbor RightNeighbor;
        public MazeNeighbor BottomNeighbor;
        public bool IsUpsideDown;
        
        public IEnumerable<MazeNeighbor> Neighbors {
            get
            {
                yield return LeftNeighbor;
                yield return RightNeighbor;
                yield return BottomNeighbor;
            }
        }
        
        public int DistanceToEntrance { get; set; } = -1;
        public IPhysicalCell PhysicalCell { get; set; }
    }
}