﻿using System.Collections.Generic;
using System.Linq;


namespace CodeBase.Maze.Creators
{
    public class TriangleMaze : IMaze
    {
        public TriangleMazeCell[][] Cells { get; }
        public MazeWall[] Walls { get; }
        public IMazeCell Entrance { get; set; }
        public IMazeCell Exit { get; set; }

        private readonly IMazeCell[] _allCells;
        
        IEnumerable<IMazeCell> IMaze.Cells => _allCells;
        IEnumerable<MazeWall> IMaze.Walls => Walls;

        
        public TriangleMaze(TriangleMazeCell[][] cells, MazeWall[] walls)
        {
            Cells = cells;
            Walls = walls;

            _allCells = Cells.SelectMany(column => column).Cast<IMazeCell>().ToArray();
        }
    }
}