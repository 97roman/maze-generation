﻿using System.Collections.Generic;
using CodeBase.Maze.Builders;


namespace CodeBase.Maze.Creators
{
    public class RectMazeCell : IMazeCell
    {
        public MazeNeighbor LeftNeighbor;
        public MazeNeighbor RightNeighbor;
        public MazeNeighbor TopNeighbor;
        public MazeNeighbor BottomNeighbor;
        
        public IEnumerable<MazeNeighbor> Neighbors {
            get
            {
                yield return LeftNeighbor;
                yield return RightNeighbor;
                yield return TopNeighbor;
                yield return BottomNeighbor;
            }
        }
        
        public int DistanceToEntrance { get; set; } = -1;
        public IPhysicalCell PhysicalCell { get; set; }
    }
}