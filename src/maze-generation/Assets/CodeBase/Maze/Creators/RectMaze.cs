﻿using System.Collections.Generic;


namespace CodeBase.Maze.Creators
{
    public class RectMaze : IMaze
    {
        public RectMazeCell[,] Cells { get; }
        public MazeWall[] Walls { get; }
        public IMazeCell Entrance { get; set; }
        public IMazeCell Exit { get; set; }

        private readonly IMazeCell[] _allCells;
        
        IEnumerable<IMazeCell> IMaze.Cells => _allCells;
        IEnumerable<MazeWall> IMaze.Walls => Walls;

        public RectMaze(RectMazeCell[,] cells, MazeWall[] walls)
        {
            Cells = cells;
            Walls = walls;

            int width = cells.GetLength(0);
            int height = cells.GetLength(1);
            var allCells = new IMazeCell[width * height];
            
            for (var x = 0; x < width; x++)
                for (var y = 0; y < height; y++)
                    allCells[x * height + y] = Cells[x, y];

            _allCells = allCells;
        }
    }
}