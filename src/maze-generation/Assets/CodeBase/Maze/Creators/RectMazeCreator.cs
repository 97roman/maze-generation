﻿using System.Collections.Generic;
using CodeBase.Extensions;


namespace CodeBase.Maze.Creators
{
    public class RectMazeCreator
    {
        private readonly int _mazeWidth;
        private readonly int _mazeHeight;
        
        private RectMazeCell[,] _mazeCells;
        private List<MazeWall> _mazeWalls;

        
        public RectMazeCreator(int mazeWidth, int mazeHeight)
        {
            _mazeWidth = mazeWidth;
            _mazeHeight = mazeHeight;
        }
        
        public RectMaze CreateMaze()
        {
            _mazeCells = new RectMazeCell[_mazeWidth, _mazeHeight];
            _mazeWalls = new List<MazeWall>();

            for (var x = 0; x < _mazeWidth; x++)
                for (var y = 0; y < _mazeHeight; y++)
                    _mazeCells[x,y] = new RectMazeCell();

            for (var x = 0; x < _mazeWidth; x++)
            {
                for (var y = 0; y < _mazeHeight; y++)
                {
                    SetLeftNeighbor(x, y);
                    SetRightNeighbor(x, y);
                    SetBottomNeighbor(x, y);
                    SetTopNeighbor(x, y);
                }
            }

            return new RectMaze(_mazeCells, _mazeWalls.ToArray());
        }

        private void SetLeftNeighbor(int x, int y)
        {
            var cell = _mazeCells[x, y];

            if (x == 0)
            {
                cell.LeftNeighbor.Wall = CreateWall();
                cell.LeftNeighbor.Wall.IsEdge = true;
                cell.LeftNeighbor.Wall.IsExists = true;
                cell.LeftNeighbor.Wall.Cells[0] = cell;
            }
            else
            {
                cell.LeftNeighbor.Wall = _mazeCells[x - 1, y].RightNeighbor.Wall;
                cell.LeftNeighbor.Cell = _mazeCells[x - 1, y];
            }
        }

        private void SetRightNeighbor(int x, int y)
        {
            var cell = _mazeCells[x, y];

            cell.RightNeighbor.Wall = CreateWall();
            cell.RightNeighbor.Wall.Cells[0] = cell;
            
            if (x == _mazeWidth - 1)
            {
                cell.RightNeighbor.Wall.IsEdge = true;
                cell.RightNeighbor.Wall.IsExists = true;
            }
            else
            {
                cell.RightNeighbor.Wall.Cells[1] = _mazeCells[x + 1, y];
                cell.RightNeighbor.Cell = _mazeCells[x + 1, y];
            }
        }

        private void SetBottomNeighbor(int x, int y)
        {
            var cell = _mazeCells[x, y];

            if (y == 0)
            {
                cell.BottomNeighbor.Wall = CreateWall();
                cell.BottomNeighbor.Wall.IsEdge = true;
                cell.BottomNeighbor.Wall.IsExists = true;
                cell.BottomNeighbor.Wall.Cells[0] = cell;
            }
            else
            {
                cell.BottomNeighbor.Wall = _mazeCells[x, y - 1].TopNeighbor.Wall;
                cell.BottomNeighbor.Cell = _mazeCells[x, y - 1];
            }
        }

        private void SetTopNeighbor(int x, int y)
        {
            var cell = _mazeCells[x, y];

            cell.TopNeighbor.Wall = CreateWall();
            cell.TopNeighbor.Wall.Cells[0] = cell;
            
            if (y == _mazeHeight - 1)
            {
                cell.TopNeighbor.Wall.IsEdge = true;
                cell.TopNeighbor.Wall.IsExists = true;
            }
            else
            {
                cell.TopNeighbor.Wall.Cells[1] = _mazeCells[x, y + 1];
                cell.TopNeighbor.Cell = _mazeCells[x, y + 1];
            }
        }

        private MazeWall CreateWall()
        {
            var wall = new MazeWall();
            _mazeWalls.Add(wall);
            return wall;
        }
    }
}