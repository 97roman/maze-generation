﻿using System.Collections.Generic;
using CodeBase.Extensions;


namespace CodeBase.Maze.Creators
{
    public class TriangleMazeCreator
    {
        private readonly int _size;

        private TriangleMazeCell[][] _mazeCells;
        private List<MazeWall> _mazeWalls;

        
        public TriangleMazeCreator(int size)
        {
            _size = size;
        }

        public TriangleMaze CreateMaze()
        {
            _mazeWalls = new List<MazeWall>();

            CreateCells();

            for (var x = 0; x < _mazeCells.Length; x++)
            {
                for (var y = 0; y < _mazeCells[x].Length; y++)
                {
                    SetLeftNeighbor(x, y);
                    SetRightNeighbor(x, y);
                    SetBottomNeighbor(x, y);
                }
            }

            return new TriangleMaze(_mazeCells, _mazeWalls.ToArray());
        }

        private void CreateCells()
        {
            _mazeCells = new TriangleMazeCell[_size * 2 - 1][];
            for (var x = 0; x < _mazeCells.Length; x++)
            {
                var columnLength = x < _size ? x + 1 : _mazeCells.Length - x;
                _mazeCells[x] = new TriangleMazeCell[columnLength];

                for (var y = 0; y < columnLength; y++)
                {
                    _mazeCells[x][y] = new TriangleMazeCell();
                    _mazeCells[x][y].IsUpsideDown = (x + y) % 2 == 1;
                }
            }
        }

        private void SetLeftNeighbor(int x, int y)
        {
            var cell = _mazeCells[x][y];
            
            if (x - y == 0)
            {
                cell.LeftNeighbor.Wall = CreateWall();
                cell.LeftNeighbor.Wall.IsEdge = true;
                cell.LeftNeighbor.Wall.IsExists = true;
                cell.LeftNeighbor.Wall.Cells[0] = cell;
            }
            else
            {
                var neighborCell = cell.IsUpsideDown
                    ? _mazeCells[x + 1][y]
                    : _mazeCells[x - 1][y];

                cell.LeftNeighbor.Cell = neighborCell;

                if (neighborCell.LeftNeighbor.Wall != null)
                {
                    cell.LeftNeighbor.Wall = neighborCell.LeftNeighbor.Wall;
                }
                else
                {
                    cell.LeftNeighbor.Wall = CreateWall();
                    cell.LeftNeighbor.Wall.Cells[0] = cell;
                    cell.LeftNeighbor.Wall.Cells[1] = neighborCell;
                }
            }
        }

        private void SetRightNeighbor(int x, int y)
        {
            var cell = _mazeCells[x][y];
            
            if (x + y + 1 == _mazeCells.Length)
            {
                cell.RightNeighbor.Wall = CreateWall();
                cell.RightNeighbor.Wall.IsEdge = true;
                cell.RightNeighbor.Wall.IsExists = true;
                cell.RightNeighbor.Wall.Cells[0] = cell;
            }
            else
            {
                var neighborCell = cell.IsUpsideDown
                    ? _mazeCells[x - 1][y]
                    : _mazeCells[x + 1][y];
                
                cell.RightNeighbor.Cell = neighborCell;

                if (neighborCell.RightNeighbor.Wall != null)
                {
                    cell.RightNeighbor.Wall = neighborCell.RightNeighbor.Wall;
                }
                else
                {
                    cell.RightNeighbor.Wall = CreateWall();
                    cell.RightNeighbor.Wall.Cells[0] = cell;
                    cell.RightNeighbor.Wall.Cells[1] = neighborCell;
                }
            }
        }

        private void SetBottomNeighbor(int x, int y)
        {
            var cell = _mazeCells[x][y];

            if (y == 0 && !cell.IsUpsideDown)
            {
                cell.BottomNeighbor.Wall = CreateWall();
                cell.BottomNeighbor.Wall.IsEdge = true;
                cell.BottomNeighbor.Wall.IsExists = true;
                cell.BottomNeighbor.Wall.Cells[0] = cell;
            }
            else
            {
                var neighborCell = cell.IsUpsideDown
                    ? _mazeCells[x][y + 1]
                    : _mazeCells[x][y - 1];
                
                cell.BottomNeighbor.Cell = neighborCell;

                if (neighborCell.BottomNeighbor.Wall != null)
                {
                    cell.BottomNeighbor.Wall = neighborCell.BottomNeighbor.Wall;
                }
                else
                {
                    cell.BottomNeighbor.Wall = CreateWall();
                    cell.BottomNeighbor.Wall.Cells[0] = cell;
                    cell.BottomNeighbor.Wall.Cells[1] = neighborCell;
                }
            }
        }

        private MazeWall CreateWall()
        {
            var wall = new MazeWall();
            _mazeWalls.Add(wall);
            return wall;
        }
    }
}