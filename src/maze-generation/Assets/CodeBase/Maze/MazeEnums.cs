﻿namespace CodeBase.Maze
{
    public enum MazeFormType
    {
        Square,
        Triangle,
    }

    public enum MazeGenerationAlgorithm
    {
        RecursiveBacktracker
    }

    public enum MazeViewType
    {
        TwoDimensional,
        ThreeDimensionalThirdPerson,
        ThreeDimensionalFirstPerson,
    }
}