﻿using System.Collections.Generic;


namespace CodeBase.Maze
{
    public interface IMaze
    {
        IEnumerable<IMazeCell> Cells { get; }
        IEnumerable<MazeWall> Walls { get; }
        IMazeCell Entrance { get; set; }
        IMazeCell Exit { get; set; }
    }
}