﻿namespace CodeBase.Maze
{
    public struct MazeNeighbor
    {
        public MazeWall Wall;
        public IMazeCell Cell;
    }
}