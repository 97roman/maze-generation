﻿using System.Collections.Generic;
using System.Linq;
using CodeBase.Extensions;


namespace CodeBase.Maze.Generators
{
    public class RecursiveBacktrackerGenerator : IMazeGenerator
    {
        public void Generate(IMaze maze)
        {
            maze.SetAllInternalWalls();
            var stack = new Stack<IMazeCell>();
            
            var current = maze.Entrance;
            current.DistanceToEntrance = 0;
            stack.Push(current);

            while (stack.Count != 0)
            {
                var unvisitedCells = current.Neighbors
                    .Where(neighbor => !neighbor.Wall.IsEdge)
                    .Where(neighbor => neighbor.Cell.DistanceToEntrance < 0)
                    .ToArray();

                if (unvisitedCells.Length > 0)
                {
                    var chosenNeighbor = unvisitedCells.GetRandomElement();
                    chosenNeighbor.Wall.IsExists = false;
                    chosenNeighbor.Cell.DistanceToEntrance = current.DistanceToEntrance + 1;
                    current = chosenNeighbor.Cell;
                    stack.Push(current);
                }
                else
                {
                    current = stack.Pop();
                }
            }
        }
    }
}