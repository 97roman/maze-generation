﻿namespace CodeBase.Maze.Generators
{
    public interface IMazeGenerator
    {
        void Generate(IMaze maze);
    }
}