﻿using System.Collections.Generic;
using CodeBase.Maze.Builders;


namespace CodeBase.Maze
{
    public interface IMazeCell
    {
        IEnumerable<MazeNeighbor> Neighbors { get; }
        int DistanceToEntrance { get; set; }
        IPhysicalCell PhysicalCell { get; set; }
    }
}