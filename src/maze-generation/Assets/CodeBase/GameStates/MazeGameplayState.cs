﻿using CodeBase.RuntimeData;
using CodeBase.UI;
using Modules.ServiceManagement;
using Modules.StateMachines;
using UnityEngine;


namespace CodeBase.GameStates
{
    public class MazeGameplayState : IState
    {
        private const string ViewTag = "MazeGameplayView";

        private readonly GameStateMachine _stateMachine;
        private readonly Model _model;
        private readonly GameplayMazeBuilder _gameplayMazeBuilder;
        private MazeGameplayView _view;


        public MazeGameplayState(GameStateMachine stateMachine, Model model)
        {
            _stateMachine = stateMachine;
            _model = model;
            _gameplayMazeBuilder = ServiceLocator.Container.Construct<GameplayMazeBuilder>();
        }
        
        public void Enter()
        {
            _view = GameObject.FindWithTag(ViewTag).GetComponent<MazeGameplayView>();
            _view.Initialize(_model.MazeSettings.ViewType);
            _view.BackButtonPressed += OnBackButtonPressed;
            _view.ViewTypeChanged += _gameplayMazeBuilder.SwitchMazeViewType;
            
            _gameplayMazeBuilder.GenerateAndBuildMaze();
        }

        public void Exit()
        {
            _view.BackButtonPressed -= OnBackButtonPressed;
            _view.ViewTypeChanged -= _gameplayMazeBuilder.SwitchMazeViewType;
        }

        private void OnBackButtonPressed()
        {
            _stateMachine.Enter(States.LoadMazeConfiguration);
        }
    }
}