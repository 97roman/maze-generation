﻿using CodeBase.Services;
using Modules.StateMachines;


namespace CodeBase.GameStates
{
    public class LoadMazeGameplayState : IState
    {
        private const string MazeGameplayScene = "MazeGameplayScene";
        private readonly GameStateMachine _stateMachine;
        private readonly SceneLoader _sceneLoader;


        public LoadMazeGameplayState(GameStateMachine stateMachine, SceneLoader sceneLoader)
        {
            _stateMachine = stateMachine;
            _sceneLoader = sceneLoader;
        }
        
        public void Enter()
        {
            _sceneLoader.Load(MazeGameplayScene, OnSceneLoaded);
        }

        public void Exit()
        {
        }
        
        private void OnSceneLoaded()
        {
            _stateMachine.Enter(States.MazeGameplay);
        }
    }
}