﻿namespace CodeBase.GameStates
{
    public enum States
    {
        Bootstrap,
        LoadMazeConfiguration,
        MazeConfiguration,
        LoadMazeGameplay,
        MazeGameplay,
    }
}