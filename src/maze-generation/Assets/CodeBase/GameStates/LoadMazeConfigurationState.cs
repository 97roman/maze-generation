﻿using CodeBase.Services;
using Modules.StateMachines;


namespace CodeBase.GameStates
{
    public class LoadMazeConfigurationState : IState
    {
        private const string MazeConfigurationScene = "MazeConfigurationScene";
        private readonly GameStateMachine _stateMachine;
        private readonly SceneLoader _sceneLoader;

        
        public LoadMazeConfigurationState(GameStateMachine stateMachine, SceneLoader sceneLoader)
        {
            _stateMachine = stateMachine;
            _sceneLoader = sceneLoader;
        }

        public void Enter()
        {
            _sceneLoader.Load(MazeConfigurationScene, OnSceneLoaded);
        }

        public void Exit()
        {
        }
        
        private void OnSceneLoaded()
        {
            _stateMachine.Enter(States.MazeConfiguration);
        }
    }
}