﻿using CodeBase.RuntimeData;
using CodeBase.UI;
using Modules.StateMachines;
using UnityEngine;


namespace CodeBase.GameStates
{
    public class MazeConfigurationState : IState
    {
        private readonly GameStateMachine _stateMachine;
        private readonly Model _model;
        private MazeSettingsPresenter _settingsPresenter;


        public MazeConfigurationState(GameStateMachine stateMachine, Model model)
        {
            _stateMachine = stateMachine;
            _model = model;
        }
        
        public void Enter()
        {
            var mazeSettings = _model.MazeSettings;
            _settingsPresenter = GameObject.FindWithTag("MazeSettingsPresenter").GetComponent<MazeSettingsPresenter>();
            _settingsPresenter.Initialize(mazeSettings.Form, mazeSettings.GenerationAlgorithm, mazeSettings.ViewType);
            _settingsPresenter.ContinueButtonPressed += OnContinueButtonPressed;
        }

        public void Exit()
        {
            _model.MazeSettings.Form = _settingsPresenter.Form;
            _model.MazeSettings.GenerationAlgorithm = _settingsPresenter.Algorithm;
            _model.MazeSettings.ViewType = _settingsPresenter.ViewType;
            _settingsPresenter.ContinueButtonPressed -= OnContinueButtonPressed;
        }

        private void OnContinueButtonPressed()
        {
            _stateMachine.Enter(States.LoadMazeGameplay);
        }
    }
}