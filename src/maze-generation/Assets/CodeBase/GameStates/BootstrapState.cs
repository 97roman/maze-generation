﻿using CodeBase.RuntimeData;
using CodeBase.Services;
using CodeBase.Services.InputSystem;
using Modules.ServiceManagement;
using Modules.StateMachines;
using UnityEngine;


namespace CodeBase.GameStates
{
    public class BootstrapState : IState
    {
        private readonly GameStateMachine _stateMachine;
        private readonly Transform _monoBehaviourSingletonContainer;


        public BootstrapState(GameStateMachine stateMachine, Transform monoBehaviourSingletonContainer)
        {
            _stateMachine = stateMachine;
            _monoBehaviourSingletonContainer = monoBehaviourSingletonContainer;
            RegisterServices(); //todo it's not good that services registers it constructor instead Enter, but it's services needed in constructors of other states 
        }
        
        public void Enter()
        {
            _stateMachine.Enter(States.LoadMazeConfiguration);
            ServiceLocator.Container.Resolve<StaticDataService>().LoadMazeData();
        }

        public void Exit()
        {
        }
        
        private void RegisterServices()
        {
            var container = ServiceLocator.Container;
            RegisterSingleton<IUnityLifeCycleProvider, UnityLifeCycleProvider>();
            RegisterSingleton<ICoroutineHolder, CoroutineHolder>();
            container.Register<IInputSystem>().To<UnityInputSystem>();
            container.Register<SceneLoader>();
            container.Register<Model>();
            container.Register<StaticDataService>();
        }

        private void RegisterSingleton<TContract, TRealization>() where TRealization : MonoBehaviour, TContract
        {
            var componentObject = new GameObject(nameof(TContract));
            componentObject.transform.SetParent(_monoBehaviourSingletonContainer);
            var component = componentObject.AddComponent<TRealization>();
            
            ServiceLocator.Container.Register<TContract>().FromInstance(component);
        }
    }
}