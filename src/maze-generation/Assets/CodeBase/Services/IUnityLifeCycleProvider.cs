﻿using System;


namespace CodeBase.Services
{
    public interface IUnityLifeCycleProvider
    {
        event Action Update;
        event Action LateUpdate;
    }
}