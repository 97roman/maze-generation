﻿using System;
using UnityEngine;


namespace CodeBase.Services.InputSystem
{
    public interface IInputSystem
    {
        Vector2 MoveAxis { get; }
        Vector2 ViewAxis { get; }
        event Action Jump;
    }
}