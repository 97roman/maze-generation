﻿using System;
using UnityEngine;


namespace CodeBase.Services.InputSystem
{
    public class UnityInputSystem : IInputSystem, IDisposable
    {
        private const float ViewAxisSensitivity = 1.25f;
        
        private readonly IUnityLifeCycleProvider _lifeCycle;

        public event Action Jump;
        

        public UnityInputSystem(IUnityLifeCycleProvider lifeCycle)
        {
            _lifeCycle = lifeCycle;
            _lifeCycle.Update += OnUpdate;
        }

        public Vector2 MoveAxis =>
            new Vector2(
                    Input.GetAxisRaw("Horizontal"),
                    Input.GetAxisRaw("Vertical"))
                .normalized;

        public Vector2 ViewAxis =>
            new Vector2(
                Input.GetAxisRaw("Mouse X"),
                -Input.GetAxisRaw("Mouse Y"))
            * ViewAxisSensitivity;

        public void Dispose()
        {
            _lifeCycle.Update -= OnUpdate;
        }

        private void OnUpdate()
        {
            if (Input.GetKeyDown(KeyCode.Space))
                Jump?.Invoke();
        }
    }
}