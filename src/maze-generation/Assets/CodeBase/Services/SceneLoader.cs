﻿using System;
using System.Collections;
using UnityEngine.SceneManagement;


namespace CodeBase.Services
{
    public class SceneLoader
    {
        private readonly ICoroutineHolder _coroutineHolder;

        public SceneLoader(ICoroutineHolder coroutineHolder)
        {
            _coroutineHolder = coroutineHolder;
        }

        public void Load(string name, Action onLoaded = null) =>
            _coroutineHolder.StartCoroutine(LoadScene(name, onLoaded));

        private static IEnumerator LoadScene(string name, Action onLoaded = null)
        {
            if (SceneManager.GetActiveScene().name == name)
            {
                onLoaded?.Invoke();
                yield break;
            }

            var sceneLoadOperation = SceneManager.LoadSceneAsync(name);
            yield return sceneLoadOperation;

            onLoaded?.Invoke();
        }
    }
}