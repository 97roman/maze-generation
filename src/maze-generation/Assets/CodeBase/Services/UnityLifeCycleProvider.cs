﻿using System;
using UnityEngine;


namespace CodeBase.Services
{
    public class UnityLifeCycleProvider : MonoBehaviour, IUnityLifeCycleProvider
    {
        private Action _update;
        private Action _lateUpdate;
        
        event Action IUnityLifeCycleProvider.Update
        {
            add => _update += value;
            remove => _update -= value;
        }

        event Action IUnityLifeCycleProvider.LateUpdate
        {
            add => _lateUpdate += value;
            remove => _lateUpdate -= value;
        }


        private void Update()
        {
            _update?.Invoke();
        }

        private void LateUpdate()
        {
            _lateUpdate?.Invoke();
        }
    }
}