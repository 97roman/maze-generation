﻿using System.Collections;
using UnityEngine;


namespace CodeBase.Services
{
    public interface ICoroutineHolder
    {
        Coroutine StartCoroutine(IEnumerator coroutine);
        void StopCoroutine(Coroutine coroutine);
        void StopAllCoroutines();
    }
}