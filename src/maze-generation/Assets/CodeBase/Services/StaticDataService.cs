﻿using System.Collections.Generic;
using CodeBase.Maze;
using CodeBase.StaticData;
using UnityEngine;


namespace CodeBase.Services
{
    public class StaticDataService
    {
        private Dictionary<MazeFormType, Dictionary<MazeViewType, MazeBuildData>> _mazeBuildData = new Dictionary<MazeFormType, Dictionary<MazeViewType, MazeBuildData>>();

    
        public void LoadMazeData()
        {
            var data = Resources.LoadAll<MazeBuildData>("StaticData/MazeBuilding");

            foreach (var buildData in data)
            {
                if (!_mazeBuildData.TryGetValue(buildData.FormType, out var buildDataByViewType))
                {
                    buildDataByViewType = new Dictionary<MazeViewType, MazeBuildData>();
                    _mazeBuildData.Add(buildData.FormType, buildDataByViewType);
                }
                
                buildDataByViewType.Add(buildData.ViewType, buildData);
            }

        }

        public MazeBuildData ForMaze(MazeFormType formType, MazeViewType viewType)
        {
            return _mazeBuildData.TryGetValue(formType, out var buildDataByViewType)
                ? buildDataByViewType.TryGetValue(viewType, out var staticData)
                    ? staticData
                    : null
                : null;
        }
    }
}