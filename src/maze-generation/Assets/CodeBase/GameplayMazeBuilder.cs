﻿using System;
using System.Collections;
using CodeBase.Extensions;
using CodeBase.Maze;
using CodeBase.Maze.Builders;
using CodeBase.Maze.Creators;
using CodeBase.Maze.Generators;
using CodeBase.RuntimeData;
using CodeBase.Services;
using UnityEngine;
using Object = UnityEngine.Object;


namespace CodeBase
{
    public class GameplayMazeBuilder
    {
        private readonly Model _model;
        private readonly StaticDataService _staticData;
        private readonly ICoroutineHolder _coroutineHolder;
        private IMaze _maze;
        private GameObject _mazeRoot;
        private Vector2? _playerPosition;
        private float _playerRotation;
        private GameObject _player;


        public GameplayMazeBuilder(StaticDataService staticData, Model model, ICoroutineHolder coroutineHolder)
        {
            _staticData = staticData;
            _model = model;
            _coroutineHolder = coroutineHolder;
        }

        public void GenerateAndBuildMaze()
        {
            _maze = CreateMaze();
            var generator = CreateGenerator();
            
            _maze.SetRandomEntranceAtEdge();
            generator.Generate(_maze);
            _maze.SetFarthestExitAtEdge();

            _coroutineHolder.StartCoroutine(BuildMaze());
        }
        
        public void SwitchMazeViewType(MazeViewType viewType)
        {
            SavePlayerPositionAndRotation();
            _model.MazeSettings.ViewType = viewType;
            _coroutineHolder.StartCoroutine(BuildMaze());
        }
        

        private IEnumerator BuildMaze()
        {
            if (_mazeRoot != null)
            {
                Object.Destroy(_mazeRoot);
                yield return null;
            }

            _mazeRoot = new GameObject("MazeRoot");
            var root = _mazeRoot.transform;
            var cellsRootObject = new GameObject("Maze");
            var cellsRoot = cellsRootObject.transform;
            cellsRoot.SetParent(root);

            var buildData = _staticData.ForMaze(_model.MazeSettings.Form, _model.MazeSettings.ViewType);
            var mazeBuilder = new UniversalMazeBuilder(buildData.FormType, buildData.MazeCellPrefab, buildData.MazeExitPrefab);
            mazeBuilder.BuildMaze(_maze, cellsRoot);
            
            _player = Object.Instantiate(buildData.PlayerPrefab, root);
            WarpPlayerToSavedPoint();

            if (buildData.EnvironmentPrefab != null)
                Object.Instantiate(buildData.EnvironmentPrefab, root);

            if (buildData.CameraPrefab != null)
            {
                var cameraObject = Object.Instantiate(buildData.CameraPrefab, root);
                cameraObject.GetComponent<CameraFollow>().FollowTarget = _player.transform;
            }
        }
        
        private IMaze CreateMaze()
        {
            switch (_model.MazeSettings.Form)
            {
                case MazeFormType.Square:
                    return new RectMazeCreator(10, 10).CreateMaze();

                case MazeFormType.Triangle:
                    return new TriangleMazeCreator(10).CreateMaze();

                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        private IMazeGenerator CreateGenerator()
        {
            switch (_model.MazeSettings.GenerationAlgorithm)
            {
                case MazeGenerationAlgorithm.RecursiveBacktracker:
                    return new RecursiveBacktrackerGenerator();
                
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }
        
        private void SavePlayerPositionAndRotation()
        {
            switch (_model.MazeSettings.ViewType)
            {
                case MazeViewType.TwoDimensional:
                    _playerPosition = new Vector2(_player.transform.position.x, _player.transform.position.y);
                    _playerRotation = _player.transform.rotation.eulerAngles.z;
                    break;
                
                case MazeViewType.ThreeDimensionalThirdPerson:
                case MazeViewType.ThreeDimensionalFirstPerson:
                    _playerPosition = new Vector2(_player.transform.position.x, _player.transform.position.z);
                    _playerRotation = -_player.transform.rotation.eulerAngles.y + 90f;
                    break;
                
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        private void LoadPlayerPositionAndRotation()
        {
            switch (_model.MazeSettings.ViewType)
            {
                case MazeViewType.TwoDimensional:
                    if (_playerPosition != null)
                        _player.transform.position = new Vector3(_playerPosition.Value.x, _playerPosition.Value.y, 0f);
                    else
                        _player.transform.position = _maze.Entrance.PhysicalCell.WorldPathPoint;

                    _player.transform.rotation = Quaternion.Euler(0f, 0f, _playerRotation);
                    break;
                
                case MazeViewType.ThreeDimensionalThirdPerson:
                case MazeViewType.ThreeDimensionalFirstPerson:
                    if (_playerPosition != null)
                        _player.transform.position = new Vector3(_playerPosition.Value.x, 0f, _playerPosition.Value.y);
                    else
                        _player.transform.position = _maze.Entrance.PhysicalCell.WorldPathPoint;

                    _player.transform.rotation = Quaternion.Euler(0f, -_playerRotation + 90f, 0f);
                    break;
                
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        private void WarpPlayerToSavedPoint()
        {
            var characterController = _player.GetComponent<CharacterController>();
            
            if (characterController != null)
            {
                characterController.enabled = false;
                LoadPlayerPositionAndRotation();
                characterController.enabled = true;
            }
            else
            {
                LoadPlayerPositionAndRotation();
            }
        }
    }
}