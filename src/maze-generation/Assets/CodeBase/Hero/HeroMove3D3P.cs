﻿using CodeBase.Services.InputSystem;
using Modules.ServiceManagement;
using UnityEngine;


namespace CodeBase.Hero
{
    [RequireComponent(typeof(CharacterController))]
    public class HeroMove3D3P : MonoBehaviour
    {
        public float MovementSpeed = 10f;

        private IInputSystem _input;
        private Camera _camera;
        private CharacterController _characterController;


        private void Start()
        {
            _characterController = GetComponent<CharacterController>();
            _input = ServiceLocator.Container.Resolve<IInputSystem>();
            _camera = Camera.main;
        }

        private void Update()
        {
            Vector3 movementVector = _input.MoveAxis;
            if (movementVector.sqrMagnitude > Mathf.Epsilon)
            {
                movementVector = _camera.transform.TransformDirection(movementVector);
                movementVector.y = 0f;
                movementVector.Normalize();
                
                transform.forward = movementVector;
            }
        
            movementVector += Physics.gravity;
            _characterController.Move(MovementSpeed * movementVector * Time.deltaTime);
        }
    }
}