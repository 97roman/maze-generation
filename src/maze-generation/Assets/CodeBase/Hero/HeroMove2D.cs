﻿using CodeBase.Services.InputSystem;
using Modules.ServiceManagement;
using UnityEngine;


namespace CodeBase.Hero
{
    [RequireComponent(typeof(Rigidbody2D))]
    public class HeroMove2D : MonoBehaviour
    {
        public float Speed = 2f;
        
        private Rigidbody2D _rigidbody;
        private IInputSystem _input;


        private void Awake()
        {
            _rigidbody = GetComponent<Rigidbody2D>();
            _input = ServiceLocator.Container.Resolve<IInputSystem>();
        }

        private void FixedUpdate()
        {
            var moveVector = _input.MoveAxis;
            if (moveVector == Vector2.zero)
                return;
            
            var rotation = Mathf.Atan2(moveVector.y, moveVector.x) * Mathf.Rad2Deg;
            _rigidbody.rotation = rotation;
            
            moveVector *= Speed * Time.deltaTime;
            _rigidbody.MovePosition(_rigidbody.position + moveVector);
        }
    }
}