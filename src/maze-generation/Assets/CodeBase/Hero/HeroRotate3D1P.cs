﻿using CodeBase.Extensions;
using CodeBase.Services.InputSystem;
using Modules.ServiceManagement;
using UnityEngine;


namespace CodeBase.Hero
{
    public class HeroRotate3D1P : MonoBehaviour
    {
        private IInputSystem _input;
        private Transform _horizontalRotationTransform;
        private Transform _verticalRotationTransform;


        private void Awake()
        {
            _input = ServiceLocator.Container.Resolve<IInputSystem>();

            _horizontalRotationTransform = GetComponentInParent<CharacterController>().transform;
            _verticalRotationTransform = transform;
        }

        private void Update()
        {
            var viewAxis =_input.ViewAxis;

            if (viewAxis.x != 0f)
            {
                var rotation = _horizontalRotationTransform.rotation;
                _horizontalRotationTransform.rotation = rotation.AddY(viewAxis.x);
            }
            
            if (viewAxis.y != 0f)
            {
                var rotation = _verticalRotationTransform.rotation;
                _verticalRotationTransform.rotation = rotation.AddX(viewAxis.y);
            }
        }
    }
}