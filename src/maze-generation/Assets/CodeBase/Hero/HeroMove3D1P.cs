﻿using CodeBase.Extensions;
using CodeBase.Services.InputSystem;
using Modules.ServiceManagement;
using UnityEngine;


namespace CodeBase.Hero
{
    [RequireComponent(typeof(CharacterController))]
    public class HeroMove3D1P : MonoBehaviour
    {
        public float MovementSpeed = 10f;
        public float JumpPower = 10f;
        public float Gravity = 3f;

        private IInputSystem _input;
        private CharacterController _characterController;
        private Vector3 _movementVector;


        private void Awake()
        {
            _characterController = GetComponent<CharacterController>();
            _input = ServiceLocator.Container.Resolve<IInputSystem>();
            _input.Jump += OnJump;
        }

        private void OnJump()
        {
            if (_characterController.isGrounded)
            {
                _movementVector.y = JumpPower;
            }
        }

        private void Update()
        {
            var isGrounded = _characterController.isGrounded;
            Vector3 moveAxis = _input.MoveAxis;

            if (isGrounded && moveAxis.sqrMagnitude > Mathf.Epsilon)
            {
                moveAxis = moveAxis.SetZ(moveAxis.y).SetY(0f);
                moveAxis = transform.TransformDirection(moveAxis);
                _movementVector.x = moveAxis.x;
                _movementVector.z = moveAxis.z;
            }
            else
            {
                _movementVector.x = 0f;
                _movementVector.z = 0f;
            }

            if (!isGrounded)
                _movementVector.y -= Gravity * Time.deltaTime;
            
            _characterController.Move(MovementSpeed * _movementVector * Time.deltaTime);
        }
    }
}