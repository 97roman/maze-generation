﻿using System;
using UnityEngine;


namespace CodeBase
{
    [Obsolete("Temporary crutch, should replace with more accomplished system in future")]
    public class CursorLocker : MonoBehaviour
    {
        private void Awake()
        {
            Cursor.lockState = CursorLockMode.Locked;
        }

        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.Escape))
                Cursor.lockState = CursorLockMode.None;
            
            if (Input.GetKeyDown(KeyCode.Return))
                Cursor.lockState = CursorLockMode.Locked;    
        }
    }
}