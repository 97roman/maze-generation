﻿using CodeBase.Maze;
using UnityEngine;


namespace CodeBase.StaticData
{
    [CreateAssetMenu(menuName = "StaticData/MazeBuildData", fileName = "MazeBuildData.asset")]
    public class MazeBuildData : ScriptableObject
    {
        public MazeFormType FormType;
        public MazeViewType ViewType;
        public GameObject MazeCellPrefab;
        public GameObject MazeExitPrefab;
        public GameObject PlayerPrefab;
        public GameObject CameraPrefab;
        public GameObject EnvironmentPrefab;
    }
}