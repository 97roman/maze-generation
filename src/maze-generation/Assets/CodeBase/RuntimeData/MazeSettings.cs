﻿using CodeBase.Maze;


namespace CodeBase.RuntimeData
{
    public class MazeSettings
    {
        public MazeFormType Form;
        public MazeGenerationAlgorithm GenerationAlgorithm;
        public MazeViewType ViewType;
    }
}