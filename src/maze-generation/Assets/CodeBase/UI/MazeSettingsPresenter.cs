﻿using System;
using CodeBase.Maze;
using UnityEngine;
using UnityEngine.UI;


namespace CodeBase.UI
{
    public class MazeSettingsPresenter : MonoBehaviour
    {
        public ChooseEnumPresenterComponent ChooseFormPresenter;
        public ChooseEnumPresenterComponent ChooseAlgorithmPresenter;
        public ChooseEnumPresenterComponent ChooseViewTypePresenter;
        public Button ContinueButton;
        
        private ChooseEnumPresenter<MazeFormType> _chooseFormPresenter;
        private ChooseEnumPresenter<MazeGenerationAlgorithm> _chooseAlgorithmPresenter;
        private ChooseEnumPresenter<MazeViewType> _chooseViewTypePresenter;

        public event Action ContinueButtonPressed;
        public MazeFormType Form => _chooseFormPresenter.CurrentValue;
        public MazeGenerationAlgorithm Algorithm => _chooseAlgorithmPresenter.CurrentValue;
        public MazeViewType ViewType => _chooseViewTypePresenter.CurrentValue;


        public void Initialize(MazeFormType formType, MazeGenerationAlgorithm algorithm, MazeViewType viewType)
        {
            _chooseFormPresenter = ChooseFormPresenter.Init<MazeFormType>(formType);
            _chooseAlgorithmPresenter = ChooseAlgorithmPresenter.Init<MazeGenerationAlgorithm>(algorithm);
            _chooseViewTypePresenter = ChooseViewTypePresenter.Init<MazeViewType>(viewType);
            ContinueButton.onClick.AddListener(OnContinueButtonPressed);
        }

        private void OnDestroy()
        {
            ContinueButton.onClick.RemoveListener(OnContinueButtonPressed);
        }

        private void OnContinueButtonPressed()
        {
            ContinueButtonPressed?.Invoke();
        }
    }
}