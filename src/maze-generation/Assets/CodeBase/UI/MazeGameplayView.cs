﻿using System;
using CodeBase.Maze;
using UnityEngine;
using UnityEngine.UI;


namespace CodeBase.UI
{
    public class MazeGameplayView : MonoBehaviour
    {
        public Button BackButton;
        public ChooseEnumPresenterComponent ChooseMazeViewType;

        public event Action BackButtonPressed;
        public event Action<MazeViewType> ViewTypeChanged;

        public void Initialize(MazeViewType viewType)
        {
            BackButton.onClick.AddListener(() => BackButtonPressed?.Invoke());
            ChooseMazeViewType.Init<MazeViewType>(viewType).ValueChanged += OnViewTypeChanged;
        }

        private void OnViewTypeChanged(MazeViewType viewType)
        {
            ViewTypeChanged?.Invoke(viewType);
        }
    }
}