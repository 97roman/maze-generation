﻿using System;
using TMPro;
using UnityEngine;


namespace CodeBase.UI
{
    [RequireComponent(typeof(TMP_Dropdown))]
    public class ChooseEnumPresenterComponent : MonoBehaviour
    {
        private TMP_Dropdown _dropdown;
        private Action _destroyAction;
        
        private void Awake()
        {
            _dropdown = GetComponent<TMP_Dropdown>();
        }

        public ChooseEnumPresenter<T> Init<T>(T initialValue)
        {
            var chooseEnumPresenter = new ChooseEnumPresenter<T>(_dropdown, initialValue);
            _destroyAction = () => chooseEnumPresenter.Dispose();
            return chooseEnumPresenter;
        }

        private void OnDestroy()
        {
            _destroyAction?.Invoke();
        }
    }
}