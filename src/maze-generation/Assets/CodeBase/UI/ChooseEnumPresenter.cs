﻿using System;
using System.Linq;
using TMPro;


namespace CodeBase.UI
{
    public class ChooseEnumPresenter<T> : IDisposable
    {
        private readonly TMP_Dropdown _dropdown;
        private readonly T[] _enumValues;

        public T CurrentValue { get; private set; }
        public event Action<T> ValueChanged;


        public ChooseEnumPresenter(TMP_Dropdown dropdown, T initialValue)
        {
            _dropdown = dropdown;
            _enumValues = EnumHelper<T>.Elements;

            _dropdown.options = _enumValues.Select(x => new TMP_Dropdown.OptionData(x.ToString())).ToList();
            _dropdown.onValueChanged.AddListener(OnValueChanged);
            
            _dropdown.value = Array.IndexOf(_enumValues, initialValue);
            CurrentValue = initialValue;
        }

        private void OnValueChanged(int index)
        {
            CurrentValue = _enumValues[index];
            ValueChanged?.Invoke(CurrentValue);
        }

        public void Dispose()
        {
            _dropdown.onValueChanged.RemoveListener(OnValueChanged);
        }
    }
}