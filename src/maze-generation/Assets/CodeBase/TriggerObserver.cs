﻿using System;
using UnityEngine;


namespace CodeBase
{
    public class TriggerObserver : MonoBehaviour
    {
        public event Action<Component> TriggerEnter;
        public event Action<Component> TriggerExit;
        
        
        private void OnTriggerEnter(Collider other)
        {
            TriggerEnter?.Invoke(other);
        }

        private void OnTriggerExit(Collider other)
        {
            TriggerExit?.Invoke(other);
        }

        private void OnTriggerEnter2D(Collider2D other)
        {
            TriggerEnter?.Invoke(other);
        }

        private void OnTriggerExit2D(Collider2D other)
        {
            TriggerExit?.Invoke(other);
        }
    }
}