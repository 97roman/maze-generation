﻿using System.Collections.Generic;
using CodeBase.GameStates;
using CodeBase.Services;
using Modules.ServiceManagement;
using Modules.StateMachines;
using UnityEngine;


namespace CodeBase
{
    public class Bootstrap : MonoBehaviour, ICoroutineHolder
    {
        public const string SceneThatNeedBeLoaded = "SceneThatNeedBeLoaded";

        private readonly Container _serviceLocator = ServiceLocator.Container;
        private GameStateMachine _stateMachine;


        private void Start()
        {
            DontDestroyOnLoad(gameObject);
            
            InitializeStateMachine();
            _stateMachine.Enter(States.Bootstrap);
            
            //var sceneLoader = ServiceLocator.Container.Resolve<SceneLoader>();
            //sceneLoader.Load(PlayerPrefs.GetString(SceneThatNeedBeLoaded));
        }

        private void InitializeStateMachine()
        {
            _stateMachine = new GameStateMachine();

            var states = new Dictionary<States, IState>
            {
                [States.Bootstrap] = new BootstrapState(_stateMachine, transform),
                [States.LoadMazeConfiguration] = _serviceLocator.Construct<LoadMazeConfigurationState>(_stateMachine),
                [States.LoadMazeGameplay] = _serviceLocator.Construct<LoadMazeGameplayState>(_stateMachine),
                [States.MazeConfiguration] = _serviceLocator.Construct<MazeConfigurationState>(_stateMachine),
                [States.MazeGameplay] = _serviceLocator.Construct<MazeGameplayState>(_stateMachine),
            };

            _stateMachine.InitStates(states);
        }
    }
}