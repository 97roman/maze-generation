﻿using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;
using UnityEngine.SceneManagement;


namespace CodeBase.Editor
{
    [InitializeOnLoad]
    public static class EndlessBootstrapper
    {
        private const string BootstrapScene = "Assets/Scenes/BootstrapScene.unity";
        private static readonly bool DoMagic = true;
        

        static EndlessBootstrapper()
        {
            if (!DoMagic)
                return;
            
            EditorApplication.playModeStateChanged += OnplayModeStateChanged;

            if (EditorSceneManager.playModeStartScene == null)
                SetPlayModeStartScene();
        }

        private static void SetPlayModeStartScene()
        {
            var scene = AssetDatabase.LoadAssetAtPath<SceneAsset>(BootstrapScene);
            EditorSceneManager.playModeStartScene = scene;
        }

        private static void OnplayModeStateChanged(PlayModeStateChange state)
        {
            if (state != PlayModeStateChange.ExitingEditMode)
                return;
            
            var currentScene = SceneManager.GetActiveScene();
            PlayerPrefs.SetString(Bootstrap.SceneThatNeedBeLoaded, currentScene.name);
        }
    }
}