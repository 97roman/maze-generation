﻿using System;


namespace CodeBase
{
    public static class EnumHelper<T>
    {
        static EnumHelper()
        {
            var enumValues = Enum.GetValues(typeof(T));

            Elements = new T[enumValues.Length];
            for (var i = 0; i < enumValues.Length; i++)
                Elements[i] = (T)enumValues.GetValue(i);
        }

        public static T[] Elements { get; }
    }
}