﻿using System.Collections.Generic;
using UnityEngine;


namespace CodeBase.Extensions
{
    public static class CollectionExtensions
    {
        public static T GetRandomElement<T>(this IReadOnlyList<T> collection)
        {
            var index = Random.Range(0, collection.Count);
            return collection[index];
        }
    }
}