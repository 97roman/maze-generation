﻿using System.Collections.Generic;
using System.Linq;
using CodeBase.Maze;
using UnityEngine;

namespace CodeBase.Extensions
{
    public static class MazeExtensions
    {
        public static void SetAllInternalWalls(this IMaze maze)
        {
            foreach (var wall in maze.Walls.Where(wall => !wall.IsEdge))
                wall.IsExists = true;
        }

        public static void SetRandomEntranceAtEdge(this IMaze maze)
        {
            var edgeCells = maze.Cells
                .Where(cell => cell.Neighbors.Any(neighbor => neighbor.Wall.IsEdge))
                .ToArray();

            maze.Entrance = edgeCells.GetRandomElement();
            //maze.Entrance.RemoveRandomEdgeWall();
        }

        public static void SetFarthestExitAtEdge(this IMaze maze)
        {
            var edgeCells = maze.Cells
                .Where(cell => cell.Neighbors.Any(neighbor => neighbor.Wall.IsEdge))
                .ToArray();

            var farthestCell = edgeCells[0];
            for (var i = 1; i < edgeCells.Length; i++)
                if (edgeCells[i].DistanceToEntrance > farthestCell.DistanceToEntrance)
                    farthestCell = edgeCells[i];

            maze.Exit = farthestCell;
            maze.Exit.RemoveRandomEdgeWall();
        }

        public static void RemoveRandomEdgeWall(this IMazeCell mazeEntrance)
        {
            mazeEntrance.Neighbors
                .Where(neighbor => neighbor.Wall.IsEdge)
                .ToArray().GetRandomElement()
                .Wall.IsExists = false;
        }

        public static IMazeCell[] GetWalkthroughPath(this IMaze maze)
        {
            var stack = new Stack<IMazeCell>();
            var current = maze.Exit;
            stack.Push(current);

            while (current.DistanceToEntrance > 0)
            {
                var nextCell = current.Neighbors
                    .Where(neighbor => !neighbor.Wall.IsEdge)
                    .Select(neighbor => neighbor.Cell)
                    .First(cell => cell.DistanceToEntrance == current.DistanceToEntrance - 1);

                current = nextCell;
                stack.Push(current);
            }

            return stack.ToArray();
        }
    }
}