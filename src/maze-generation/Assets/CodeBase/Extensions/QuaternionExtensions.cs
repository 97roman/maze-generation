﻿using UnityEngine;


namespace CodeBase.Extensions
{
    public static class QuaternionExtensions
    {
        public static Quaternion AddX(this Quaternion rotation, float angle)
        {
            var eulerAngles = rotation.eulerAngles;
            eulerAngles.x += angle;
            rotation = Quaternion.Euler(eulerAngles);
            return rotation;
        }
        
        public static Quaternion AddY(this Quaternion rotation, float angle)
        {
            var eulerAngles = rotation.eulerAngles;
            eulerAngles.y += angle;
            rotation = Quaternion.Euler(eulerAngles);
            return rotation;
        }
        
        public static Quaternion AddZ(this Quaternion rotation, float angle)
        {
            var eulerAngles = rotation.eulerAngles;
            eulerAngles.z += angle;
            rotation = Quaternion.Euler(eulerAngles);
            return rotation;
        }
    }
}