﻿namespace Modules.ServiceManagement
{
    public class ServiceContextBuilder<TService>
    {
        private readonly ServiceContext _serviceContext;

        internal ServiceContextBuilder(ServiceContext serviceContext) =>
            _serviceContext = serviceContext;

        public ServiceContextBuilder<TService> To<T>() where T : TService
        {
            _serviceContext.DestinationType = typeof(T);
            return this;
        }

        public ServiceContextBuilder<TService> FromInstance(TService instance)
        {
            _serviceContext.DestinationType = instance.GetType();
            _serviceContext.LifeType = LifeType.Singleton;
            _serviceContext.Instance = instance;
            return this;
        }

        public ServiceContextBuilder<TService> AsTransient()
        {
            _serviceContext.LifeType = LifeType.Transient;
            return this;
        }

        public ServiceContextBuilder<TService> AsSingle()
        {
            _serviceContext.LifeType = LifeType.Singleton;
            return this;
        }
    }
}