﻿using System;
using System.Linq;
using System.Reflection;


namespace Modules.ServiceManagement
{
    public class ConstructorInformation
    {
        public ConstructorInfo Constructor;
        public Type[] InvokeParameters;


        public ConstructorInformation(ConstructorInfo constructor)
        {
            Constructor = constructor;
            InvokeParameters = constructor.GetParameters().Select(x => x.ParameterType).ToArray();
        }
    }
}