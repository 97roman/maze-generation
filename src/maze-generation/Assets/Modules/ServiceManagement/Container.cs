﻿using System;
using System.Collections.Generic;
using System.Linq;


namespace Modules.ServiceManagement
{
    public class Container
    {
        private readonly Dictionary<Type, ServiceContext> _services = new Dictionary<Type, ServiceContext>();
        private readonly Stack<Type> _typesInProcessing = new Stack<Type>();

        
        public ServiceContextBuilder<T> Register<T>()
        {
            var type = typeof(T);
            if (_services.ContainsKey(type))
                throw new ServiceLocatorException($"Type {type} is already registered");

            var context = new ServiceContext(type);
            _services.Add(type, context);
            return new ServiceContextBuilder<T>(context);
        }

        public T Resolve<T>()
        {
            _typesInProcessing.Clear();
            return (T) ResolveType(typeof(T));
        }

        public void Dispose<T>()
        {
            var type = typeof(T);
            if (!_services.TryGetValue(type, out var context))
                throw new ServiceLocatorException($"Can't dispose not registered type {type}");

            context.Dispose();
            _services.Remove(type);
        }

        public T Construct<T>(params object[] definedArguments)
        {
            var constructorInfo = typeof(T).GetSimplestPublicConstructor();

            var invokeArguments = constructorInfo.InvokeParameters
                .Select(GetFromDefinedArgumentsOrResolve)
                .ToArray();

            return (T)constructorInfo.Constructor.Invoke(invokeArguments);

            object GetFromDefinedArgumentsOrResolve(Type type) =>
                definedArguments.FirstOrDefault(x => x.GetType() == type) ?? ResolveType(type);
        }


        private object ResolveType(Type type)
        {
            if (_typesInProcessing.Contains(type))
                throw new ServiceLocatorException($"Recursion dependencies. Secondary request of {type}");

            _typesInProcessing.Push(type);

            if (!_services.TryGetValue(type, out var context))
                throw new ServiceLocatorException($"Can't resolve not registered type {type}");

            if (context.DestinationType == null)
                throw new ServiceLocatorException($"Current type ({type}) not bind to concrete type");

            object resolvedType;

            switch (context.LifeType)
            {
                case LifeType.None:
                    throw new ServiceLocatorException($"Not set life type for type {type}");

                case LifeType.Transient:
                    resolvedType = CreateInstanceOfType(context.DestinationTypeConstructor);
                    break;

                case LifeType.Singleton:
                    resolvedType = EnsureSingletonExists(context);
                    break;

                default:
                    throw new ArgumentOutOfRangeException();
            }

            _typesInProcessing.Pop();
            return resolvedType;
        }

        private object EnsureSingletonExists(ServiceContext context) =>
            context.Instance ?? (context.Instance = CreateInstanceOfType(context.DestinationTypeConstructor));

        private object CreateInstanceOfType(ConstructorInformation constructorInfo)
        {
            var invokeArguments = constructorInfo.InvokeParameters.Select(ResolveType).ToArray();
            return constructorInfo.Constructor.Invoke(invokeArguments);
        }
    }
}