﻿using System;


namespace Modules.ServiceManagement
{
    internal class ServiceContext
    {
        public LifeType LifeType;
        public Type DestinationType;
        public object Instance;

        private ConstructorInformation _constructorInfo;
        

        public ServiceContext(Type destinationType)
        {
            DestinationType = destinationType;
            LifeType = LifeType.Singleton;
        }

        public void Dispose()
        {
            if (Instance == null)
                return;

            (Instance as IDisposable)?.Dispose();
            Instance = null;
        }

        public ConstructorInformation DestinationTypeConstructor =>
            _constructorInfo ?? (_constructorInfo = DestinationType.GetSimplestPublicConstructor());
    }
}