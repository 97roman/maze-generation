﻿using System;


namespace Modules.ServiceManagement
{
    public class ServiceLocatorException : Exception
    {
        public ServiceLocatorException(string message)
            : base("ServiceLocatorException: " + message)
        {
        }
    }
}