﻿using System;
using System.Linq;
using System.Reflection;


namespace Modules.ServiceManagement
{
    public static class Extensions
    {
        public static ConstructorInformation GetSimplestPublicConstructor(this Type type) =>
            type.GetConstructors(BindingFlags.Instance | BindingFlags.Public)
                .Select(x => new ConstructorInformation(x))
                .OrderBy(x => x.InvokeParameters.Length)
                .First();
    }
}