﻿namespace Modules.ServiceManagement
{
    public enum LifeType
    {
        None,
        Transient,
        Singleton,
    }
}