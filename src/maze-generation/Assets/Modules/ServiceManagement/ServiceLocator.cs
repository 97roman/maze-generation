﻿namespace Modules.ServiceManagement
{
    public class ServiceLocator
    {
        private static Container _mainContainer;

        public static Container Container =>
            _mainContainer = _mainContainer ?? (_mainContainer = new Container());
    }
}