﻿namespace Modules.StateMachines
{
    public interface IState
    {
        void Enter();
        void Exit();
    }
}