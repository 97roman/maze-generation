﻿using System.Collections.Generic;


namespace Modules.StateMachines
{
    public class StateMachine<TEnum>
    {
        private Dictionary<TEnum, IState> _states;
        private IState _activeState;
        

        public void InitStates(Dictionary<TEnum, IState> states)
        {
            _states = states;
        }
        
        public void Enter(TEnum state)
        {
            _activeState?.Exit();
            _activeState = _states[state];
            _activeState.Enter();
        }
    }
}
